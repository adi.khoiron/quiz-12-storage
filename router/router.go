package router

import (
	"go-simple-template/dto"
	"go-simple-template/handler"
	"go-simple-template/service"
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func NewRouter(s service.ServiceInterface) *echo.Echo {
	e := echo.New()
	e.Use(middleware.Logger())

	handler := handler.NewHandler(s)

	InitRouter(e, handler)

	return e
}

func InitRouter(e *echo.Echo, h *handler.Handler) {
	e.GET("/", func(c echo.Context) error {
		return c.JSON(http.StatusOK, dto.ApiResponse{
			Code:    http.StatusOK,
			Message: "Welcome to Go Simple Template",
		})
	})

	e.GET("/ping", h.Ping)
	e.POST("/document/upload", h.UploadDocument)
	e.GET("/document/download", h.DownloadDocument)
	e.DELETE("/document/download", h.DeleteDocument)
}
