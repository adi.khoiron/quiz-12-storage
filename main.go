package main

import (
	"go-simple-template/config"
	"go-simple-template/database"
	"go-simple-template/pkg/logger"
	"go-simple-template/pkg/storagex"

	// gcsstoragex "go-simple-template/pkg/storagex/gcs"

	miniostoragex "go-simple-template/pkg/storagex/minio"

	"go-simple-template/repository"
	"go-simple-template/router"
	"go-simple-template/server"
	"go-simple-template/service"

	"github.com/joho/godotenv"
)

func main() {
	log := logger.NewLogger().Logger.With().Str("pkg", "main").Logger()

	if err := godotenv.Load(); err != nil {
		log.Fatal().Err(err).Msg("Failed to load env file")
		panic(err)
	}

	cfg := config.NewConfig()

	db, err := database.NewConnection(cfg)
	if err != nil {
		log.Fatal().Err(err).Msg("Failed to connect to database")
	}

	err = database.AutoMigrate(db)
	if err != nil {
		log.Fatal().Err(err).Msg("Failed to migrate database")
	}

	repo := repository.NewRepository().WithDB(db)

	minio, err := miniostoragex.NewMinio(cfg)
	if err != nil {
		log.Fatal().Err(err).Msg("Failed to connect to minio")
	}
	storage := storagex.NewStorage(minio)

	// gcs, err := gcsstoragex.NewCGS(cfg)
	// if err != nil {
	// 	log.Fatal().Err(err).Msg("Failed to connect to gcs")
	// }
	// storage := storagex.NewStorage(gcs)

	service := service.NewService().WithStorage(storage).WithRepo(repo)
	router := router.NewRouter(service)

	server := server.NewServer(cfg, router)

	log.Info().Msgf("Server started at http://%s:%d", cfg.AppHost, cfg.AppPort)
	log.Fatal().Err(server.ListenAndServe()).Msg("Failed to start the server")
}
