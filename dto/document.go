package dto

type DocumentDownload struct {
	FileName string
	FileExt  string
}
