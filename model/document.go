package model

import (
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"time"

	"gorm.io/gorm"
)

type Document struct {
	ID            int64  `gorm:"primarykey"`
	Name          string `gorm:"unique"`
	DocumentToken string `gorm:"unique"`
	CreatedAt     time.Time
	UpdatedAt     time.Time
	DeletedAt     gorm.DeletedAt `gorm:"index"`
}

func (d *Document) BeforeCreate(tx *gorm.DB) error {
	d.CreatedAt = time.Now()
	d.UpdatedAt = time.Now()

	documentToken := fmt.Sprintf("%s:%s", d.CreatedAt.String(), d.Name)
	fmt.Println(documentToken)
	hash := sha256.Sum256([]byte(documentToken))
	hashString := hex.EncodeToString(hash[:])

	d.DocumentToken = fmt.Sprintf("DOC-%s", hashString)

	return nil
}
