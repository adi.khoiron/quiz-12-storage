package service

import (
	"bytes"
	"context"
	"go-simple-template/model"
	"go-simple-template/pkg/storagex"
)

func (s *service) Upload(ctx context.Context, file *storagex.FileUploadObject) error {
	document := model.Document{
		Name: file.FileName,
	}

	err := s.repo.StoreDocument(&document)
	if err != nil {
		logService.Error().Err(err).Msg("failed store document")
		return err
	}

	file.FileName = document.DocumentToken
	err = s.storage.Client.Upload(ctx, file)
	if err != nil {
		logService.Error().Err(err).Msg("failed upload file")
		return err
	}

	return nil
}

func (s *service) Download(ctx context.Context, fileName string) (*bytes.Buffer, error) {
	document, err := s.repo.GetDocument(fileName)
	if err != nil {
		logService.Error().Err(err).Msg("failed get document")
		return nil, err
	}

	if document == nil {
		return nil, nil
	}

	buff, err := s.storage.Client.Download(ctx, document.DocumentToken)
	if err != nil {
		logService.Error().Err(err).Msg("failed download file")
		return nil, err
	}

	return buff, nil
}

func (s *service) GetDocument(documentName string) (*model.Document, error) {
	existDocument, err := s.repo.GetDocument(documentName)
	if err != nil {
		logService.Error().Err(err).Msg("failed get document")
		return nil, err
	}

	return existDocument, nil
}

func (s *service) Delete(ctx context.Context, documentToken string) error {
	err := s.storage.Client.Delete(ctx, documentToken)
	if err != nil {
		logService.Error().Err(err).Msg("failed delete file")
		return err
	}

	err = s.repo.DeleteDocument(documentToken)
	if err != nil {
		logService.Error().Err(err).Msg("failed delete document")
		return err
	}

	return nil
}
