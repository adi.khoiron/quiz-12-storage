package service

import (
	"bytes"
	"context"
	"go-simple-template/model"
	"go-simple-template/pkg/logger"
	"go-simple-template/pkg/storagex"
	"go-simple-template/repository"

	"github.com/hibiken/asynq"
)

type service struct {
	repo    repository.RepositoryInterface
	queue   *asynq.Client
	storage *storagex.Storage
}

var (
	logService = logger.NewLogger().Logger.With().Str("pkg", "service").Logger()
)

func NewService() *service {
	return &service{}
}

type ServiceInterface interface {
	Ping() error
	Upload(ctx context.Context, file *storagex.FileUploadObject) error
	Download(ctx context.Context, fileName string) (*bytes.Buffer, error)
	GetDocument(fileName string) (*model.Document, error)
	Delete(ctx context.Context, documentToken string) error
}

func (s *service) WithRepo(repo repository.RepositoryInterface) *service {
	s.repo = repo
	return s
}

func (s *service) WithQueue(queue *asynq.Client) *service {
	s.queue = queue
	return s
}

func (s *service) WithStorage(Storage *storagex.Storage) *service {
	s.storage = Storage
	return s
}
