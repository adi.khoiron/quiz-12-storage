package handler

import (
	"fmt"
	"go-simple-template/dto"
	"go-simple-template/pkg/storagex"
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/timemore/foundation/media"
)

func (h *Handler) UploadDocument(c echo.Context) error {
	file, err := c.FormFile("document")
	if err != nil {
		return c.JSON(http.StatusUnprocessableEntity, dto.ApiResponse{
			Code:    http.StatusUnprocessableEntity,
			Message: "Can't read document file",
		})
	}

	source, err := file.Open()
	if err != nil {
		return c.JSON(http.StatusUnprocessableEntity, dto.ApiResponse{
			Code:    http.StatusUnprocessableEntity,
			Message: "Can't open document file",
		})
	}
	defer source.Close()

	object := storagex.FileUploadObject{
		FileName: file.Filename,
		File:     source,
	}

	ctx := c.Request().Context()

	existDoc, err := h.service.GetDocument(file.Filename)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, dto.ApiResponse{
			Code:    http.StatusInternalServerError,
			Message: "Failed to get document",
		})
	}

	if existDoc != nil {
		return c.JSON(http.StatusConflict, dto.ApiResponse{
			Code:    http.StatusConflict,
			Message: fmt.Sprintf("Document %s already exist", file.Filename),
		})
	}

	err = h.service.Upload(ctx, &object)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, dto.ApiResponse{
			Code:    http.StatusInternalServerError,
			Message: "Failed to upload document",
		})
	}

	return c.JSON(http.StatusOK, dto.ApiResponse{
		Code:    http.StatusOK,
		Message: "Document uploaded successfully",
	})
}

func (h *Handler) DownloadDocument(c echo.Context) error {
	ctx := c.Request().Context()
	filename := c.QueryParam("filename")

	buff, err := h.service.Download(ctx, filename)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, dto.ApiResponse{
			Code:    http.StatusInternalServerError,
			Message: "Failed to download document",
		})
	}

	if buff == nil || buff.Len() <= 0 {
		return c.JSON(http.StatusNotFound, dto.ApiResponse{
			Code:    http.StatusNotFound,
			Message: fmt.Sprintf("Document %s not found", filename),
		})
	}

	contentType := media.DetectType(buff.Bytes())

	c.Response().Header().Set("Content-Type", contentType)
	c.Response().Header().Set("Content-Disposition", "attachment;filename="+filename)
	c.Response().Header().Set("Content-Transfer-Encoding", "binary")

	_, err = c.Response().Write(buff.Bytes())
	if err != nil {
		return c.JSON(http.StatusInternalServerError, dto.ApiResponse{
			Code:    http.StatusInternalServerError,
			Message: "Failed to download document",
		})
	}

	return nil
}

func (h *Handler) DeleteDocument(c echo.Context) error {
	ctx := c.Request().Context()
	filename := c.QueryParam("filename")

	existDoc, err := h.service.GetDocument(filename)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, dto.ApiResponse{
			Code:    http.StatusInternalServerError,
			Message: "Failed to get document",
		})
	}
	
	if existDoc == nil {
		return c.JSON(http.StatusNotFound, dto.ApiResponse{
			Code:    http.StatusNotFound,
			Message: fmt.Sprintf("Document %s not found", filename),
		})
	}

	err = h.service.Delete(ctx, existDoc.DocumentToken)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, dto.ApiResponse{
			Code:    http.StatusInternalServerError,
			Message: "Failed to delete document",
		})
	}

	return c.JSON(http.StatusOK, dto.ApiResponse{
		Code:    http.StatusOK,
		Message: "Document deleted successfully",
	})
}
