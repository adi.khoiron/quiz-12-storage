package repository

import (
	"go-simple-template/model"

	"gorm.io/gorm"
)

func (r *repository) StoreDocument(document *model.Document) error {
	return r.db.Model(&model.Document{}).Save(document).Error
}

func (r *repository) GetDocument(documentName string) (*model.Document, error) {
	document := &model.Document{}
	err := r.db.Model(&model.Document{}).Where("name = ?", documentName).First(document).Error
	if err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, nil
		}

		return nil, err
	}

	return document, nil
}

func (r *repository) DeleteDocument(documentToken string) error {
	return r.db.Model(&model.Document{}).Unscoped().Where("document_token = ?", documentToken).Delete(&model.Document{
		DocumentToken: documentToken,
	}).Error
}
