# storage-service

This service is used to manage documents to storage. You can choose storage driver between google cloud platform (GCS) or MinIO.

### Available featuress :

- Upload document
- Download Document

### How To Use?

1. Begin by cloning the repository to your local machine.
2. Set up your database.
3. Create an environment file by duplicating the .env.example file and filling in the required values for each service according to your needs, including your GCS or MinIO credentials
4. Install the necessary dependencies.

```
go mod tidy
```

5. Run the program

```
go run main.go
```

6. Test liveness

```
curl --location 'localhost:8082/ping'
```

7. You can do testing service with postman collection [here](https://app.getpostman.com/run-collection/27951005-072b152a-6e40-4257-a95b-88dc0409e502?action=collection%2Ffork&source=rip_markdown&collection-url=entityId%3D27951005-072b152a-6e40-4257-a95b-88dc0409e502%26entityType%3Dcollection%26workspaceId%3D0d7bc192-c451-4fd5-833f-7656411157d4)
